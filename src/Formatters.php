<?php
/** @noinspection ALL */


class Formatters
{
    /**
     * Provides consistent formatting for phone numbers.
     *
     * @param string $phone
     * @return string
     */
    public function FormatPhone($phone)
    {
        # hold all unnecessary values which might be present in input
        $discard_characters = array("-", " ", ".");

        $phone = str_replace($discard_characters, '', $phone);
        return '(' . substr($phone, 0, 3) . ') ' . substr($phone, 3, 3) . '-' . substr($phone, 6);
    }

    /**
     * Returns a singular or plural string, based on the quantity.
     *
     * @param string $singular_string
     * @param int|float $quantity
     * @return string
     */
    public function Pluralize($singular_string, $quantity)
    {   
        # keep track of vowel characters for assigning plural forms properly
        $vowels = array('a', 'e', 'i', 'o', 'u');

        # properly assign plural form for 'y' endings
        if($quantity == 0 || $quantity > 1) {
            $end_string = substr($singular_string, -2, 2);
            if($end_string[1] == 'y'){
                if(!in_array($end_string[0], $vowels)){
                    $pluralized = substr($singular_string, 0, strlen($singular_string)-1) . "ies";
                    return $pluralized;
                }
            }
            $pluralized = $singular_string . 's';
            return $pluralized;
        }

        # return the singular form of string if found to not be plural
        return $singular_string;
    }
}
?>